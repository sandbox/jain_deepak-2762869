<?php

namespace Drupal\node_generator\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\field\FieldConfigInterface;
use \Drupal\node\Entity\Node;
use Drupal;

/**
 * Form constructor for node_generator_input_form.
 */
class ConfigurationForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'node_generator_input_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['content_pattern'] = array(
      '#type' => 'textfield',
      '#title' => t('Content Pattern'),
      '#required' => TRUE,
      '#description' => t('element.classname') . '<strong>(like h1.classname) or element#id(like div#foo)</strong>' . t('For multiple patterns, seperate them by \',\''),
    );
    $form['title_pattern'] = array(
      '#type' => 'textfield',
      '#title' => t('Title Pattern'),
      '#required' => TRUE,
      '#description' => t('element.classname') . '<strong>(like h1.classname) or element#id(like div#foo)</strong>' . ('For multiple patterns, seperate them by \',\''),
    );
    // Get all content types list.
    $content_types = node_type_get_names();
    foreach ($content_types as $key => $val) {
      // Check if given content type contains body field or not.
      $ctype_fields = $this->contentTypeFields($key);
      if (!isset($ctype_fields['body'])) {
        unset($content_types[$key]);
      }
    }
    $form['ctype_setting'] = array(
      '#type' => 'fieldset',
      '#title' => t('Content Type Settings'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#prefix' => '<div id="node-category">',
      '#suffix' => '</div>',
    );
    $form['ctype_setting']['select_content_type'] = array(
      '#type' => 'select',
      '#options' => array('' => t('Select')) + $content_types,
      '#title' => t('Select Content Type'),
      '#required' => TRUE,
      '#description' => t('Select content type for which you want to create the node.'),
      '#ajax' => array(
        'callback' => array($this, 'loadCategory'),
        'wrapper' => 'node-category',
        'event' => 'change',
        'progress' => array(
          'type' => 'throbber',
          'message' => t('loading...'),
        ),
      ),
    );
    $form['ctype_setting']['category_field'] = array(
      '#type' => 'select',
      '#options' => array('' => 'select'),
      '#title' => t('Select Field For Category'),
      '#access' => FALSE,
      '#description' => t('Select Term Refrence Field for Selected Content Type For which you want to create the category.'),
    );
    $form['ctype_setting']['tag_field'] = array(
      '#type' => 'select',
      '#options' => array('' => t('Select')),
      '#title' => t('Select Field For Tags'),
      '#access' => FALSE,
      '#description' => t('Select Term Refrence Field for Selected Content Type For which you want to create the Tags.'),
    );
    $form['ctype_setting']['category_pattern'] = array(
      '#type' => 'textfield',
      '#title' => t('Category Pattern'),
      '#access' => FALSE,
      '#description' => t('element.classname') . '<strong>(like h1.classname) or element#id(like div#foo)</strong>' . ('For multiple patterns, seperate them by \',\''),
    );
    $form['ctype_setting']['tag_pattern'] = array(
      '#type' => 'textfield',
      '#title' => t('Tag Pattern'),
      '#access' => FALSE,
      '#description' => t('element.classname') . '<strong>(like h1.classname) or element#id(like div#foo)</strong>' . ('For multiple patterns, seperate them by \',\''),
    );
    $form['date_pattern'] = array(
      '#type' => 'textfield',
      '#title' => t('Post Date Pattern'),
      '#required' => FALSE,
      '#description' => t('element.classname') . '<strong>(like h1.classname) or element#id(like div#foo)</strong>' . ('For multiple patterns, seperate them by \',\''),
    );
    $form['upload_file'] = array(
      '#type' => 'managed_file',
      '#title' => t('Upload File'),
      '#required' => FALSE,
      '#description' => t('Allowed Extensions: xml'),
      '#upload_location' => 'public://node_generator/',
      '#upload_validators' => array(
        'file_validate_extensions' => array('xml'),
        'file_validate_size' => array(1 * 1024 * 1024),
      ),
    );
    $form['xml_file_url'] = array(
      '#type' => 'textfield',
      '#title' => t('External URL Path Of XML File.'),
      '#required' => FALSE,
    );
    $form['replace_domain'] = array(
      '#type' => 'checkbox',
      '#prefix' => '<b>Do You Want to Replace Domain.</b>',
      '#default_value' => 0,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );
    $form['information'] = array(
      '#markup' => '<div><strong>' . t('Note:- Please use pattern only for class or id') . '</strong></div>',
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getValue('xml_file_url')) && empty($form_state->getValue('upload_file'))) {
      $form_state->setErrorByName('upload_file', $this->t('Please Select Atleast 1 Field.'));
      $form_state->setErrorByName('xml_file_url', $this->t('Please Select Atleast 1 Field.'));
    }
    $check_url = UrlHelper::isValid($form_state->getValue('xml_file_url'), $absolute = TRUE);
    if (!$check_url && $form_state->getValue('xml_file_url') != '') {
      $form_state->setErrorByName('xml_file_url', $this->t('Please Enter Valid URL.'));
    } elseif (!empty($form_state->getValue('xml_file_url'))) {
      $xml = $this->nodeGeneratorGetContentFromXmlLoc($form_state->getValue('xml_file_url'));
      $validate_xml = simplexml_load_string($xml);
      if (!$validate_xml) {
        $form_state->setErrorByName('xml_file_url', $this->t('Not Valid XML.'));
      }
    } elseif (empty($form_state->getValue('xml_file_url')) && !empty($form_state->getValue('upload_file'))) {
      $x = $form_state->getValue('upload_file');
      $f = file_load($x[0]);
      $urll = file_create_url($f);
      $xml = $this->nodeGeneratorGetContentFromXmlLoc($urll);
      $validate_xml = simplexml_load_string($xml);
      if (!$validate_xml) {
        $form_state->setErrorByName('edit_upload_file', $this->t('Uploded File Is Not Valid XML.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    //$this->node_generator_node_creation();
    $batch = array(
      'title' => t('Parsing XML File...'),
      'error_message' => t('An error occurred during processing'),
      'init_message' => t('Creating'),
      'operations' => array(),
      'file' => drupal_get_path('module', 'node_generator') . '/include/batch.inc',
      'finished' => '_node_generator_batch_finished',
    );
    if (!empty($form_state->getValue('xml_file_url'))) {
      $response_xml_data = $this->nodeGeneratorGetContentFromXmlLoc($form_state->getValue('xml_file_url'));
      if ($response_xml_data) {
        $xml = simplexml_load_string($response_xml_data);
      }
    } else {
      $xml = simplexml_load_file(drupal_realpath(file_load($form_state->getValue('upload_file'))->uri));
    }
    foreach ($xml as $url) {
      $param = array(
        'url' => (string) $url->loc,
        'type' => $form_state->getValue('select_content_type'),
        'content_pattern' => $form_state->getValue('content_pattern'),
        'title_pattern' => $form_state->getvalue('title_pattern'),
        'category_pattern' => $form_state->getValue('category_pattern'),
        'tag_pattern' => $form_state->getValue('tag_pattern'),
        'date_pattern' => $form_state->getValue('date_pattern'),
        'category_field' => $form_state->getValue('category_field'),
        'tag_field' => $form_state->getValue('tag_field'),
        'replace_domain' => $form_state->getValue('replace_domain'),
      );
    }
    //kint($param);die;
    $this->nodeGeneratorProcessing($param);
    //$batch['operations'][] = array('node_generator_processing', (array($param)));
    //kint($batch['operations']);die;
    //  Batch Processing starts here.
    //batch_set($batch);
  }

  /**
   * Ajax callback to validate the email field.
   */
  public function loadCategory(array &$form, FormStateInterface $form_state) {
    $ctype_fields = $this->contentTypeFields($form_state->getValue('select_content_type'));
    if ($ctype_fields) {
      foreach ($ctype_fields as $ctype_field) {
        $filed_type = $ctype_field->get('field_type');
        if ($filed_type == 'entity_reference') {
          $name[] = $ctype_field->get('label');
        }
      }
    }
    if (!empty($name)) {
      $form['ctype_setting']['category_field']['#options'] = array('select' => '- Select -') + $name;
      $form['ctype_setting']['tag_field']['#options'] = array('select' => '- Select -') + $name;
      $form['ctype_setting']['category_field']['#access'] = TRUE;
      $form['ctype_setting']['tag_field']['#access'] = TRUE;
      $form['ctype_setting']['category_pattern']['#access'] = TRUE;
      $form['ctype_setting']['tag_pattern']['#access'] = TRUE;
    } else {
      $form['ctype_setting']['category_field']['#access'] = FALSE;
      $form['ctype_setting']['tag_field']['#access'] = FALSE;
      $form['ctype_setting']['category_pattern']['#access'] = FALSE;
      $form['ctype_setting']['tag_pattern']['#access'] = FALSE;
    }
    return $form['ctype_setting'];
  }

  public function contentTypeFields($contentType) {
    $entityManager = Drupal::service('entity.manager');
    $fields = [];
    if (!empty($contentType)) {
      $fields = array_filter($entityManager->getFieldDefinitions('node', $contentType), function ($field_definition) {
        return $field_definition instanceof FieldConfigInterface;
      }
      );
    }
    return $fields;
  }

  /**
   * Get html data from url using curl.
   */
  public function nodeGeneratorGetContentFromXmlLoc($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    // Proxy authenticate.
//    curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
//    curl_setopt($ch, CURLOPT_PROXY, 'proxy.cognizant.com:6050');
//    curl_setopt($ch, CURLOPT_PROXYUSERPWD, '561194:Mygame@143');
    // Proxy authenticate.
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
  }

  /**
   * Process the node generation in for each url and pattern.
   */
  public function nodeGeneratorProcessing($content, &$context) {
    module_load_include('php', 'node_generator', 'include/simple_html_dom');
    $trimed_url = trim($content['url']);
    $html = file_get_html($trimed_url);
    $valid = TRUE;
    $img = $this->nodeGeneratorGetImageFromUri($html, $content['url']);
    $get_anchor_files = $this->nodeGeneratorGetFilesFromHref($html, $content['url']);
    if (isset($content['category_pattern']) && !empty($content['category_pattern'])) {
      $category = array();
      foreach (explode(',', $content['category_pattern']) as $pattern_category) {
        foreach ($html->find($pattern_category) as $var_category) {
          $category[] = $var_category->innertext;
        }
      }
    }
    if (isset($content['tag_pattern']) && !empty($content['tag_pattern'])) {
      $tag = array();
      foreach (explode(',', $content['tag_pattern']) as $pattern_tag) {
        foreach ($html->find($pattern_tag) as $var_tag) {
          $tag[] = $var_tag->innertext;
        }
      }
    }
    if (isset($content['date_pattern']) && !empty($content['date_pattern'])) {
      $date = '';
      foreach (explode(',', $content['date_pattern']) as $pattern_date) {
        foreach ($html->find($pattern_date) as $var_date) {
          $date = $var_date->innertext;
          if (!empty($date)) {
            break;
          }
        }
        if (!empty($date)) {
          break;
        }
      }
    }
//    // For Metatags.
//    $metatags = array();
//    $page_title = '';
//    foreach ($this->nodeGeneratorGetMetatagsUrl($content['url']) as $key => $val) {
//      if (!empty($val) && in_array($key, $this->nodeGeneratorGetMetatagsList())) {
//        $metatags['und'][$key]['value'] = $key == 'robots' ? $this->nodeGeneratorAdjustRobotTags($val) : $metatags['und'][$key]['value'] = $val;
//        if ($key == 'title') {
//          $page_title = $val;
//        }
//      }
//    }
    // For Body.
    foreach (explode(',', $content['content_pattern']) as $pattern) {
      foreach ($html->find($pattern) as $var) {
        $body = $var->innertext;
        if ($content['replace_domain'] == 1) {
          $body = $this->nodeGeneratorReplaceDomainAnchor($body, $content['url']);
        }
        if (!empty($body)) {
          break;
        }
      }
      if (!empty($body)) {
        break;
      }
    }
    if (empty($body)) {
      $context['results']['message'][] = t('Unable to create the node for url @url as body is empty', array('@url' => $content['url']));
      $valid = FALSE;
    }
    // For Node Title.
    if ($valid) {
      foreach (explode(',', $content['title_pattern']) as $pattern) {
        foreach ($html->find($pattern) as $var) {
          $node_title = $var->innertext;
          if (!empty($node_title)) {
            break;
          }
        }
        if (!empty($node_title)) {
          break;
        }
      }
      if (empty($node_title)) {
        $context['results']['message'][] = t('Unable to create the node for url @url as node title is empty', array('@url' => $content['url']));
        $valid = FALSE;
      }
    }
    if ($valid) {
      // Generate the node.
      $params = array(
        'url' => $content['url'],
        'uid' => 1,
        'page_title' => $node_title,
        'body' => $body,
        'metatags' => $metatags,
        'type' => $content['type'],
        'alias' => $this->nodeGeneratorGenerateAlias($content['url']),
        'summary' => substr($body, 0, 250),
        'category' => $category,
        'tag' => $tag,
        'date' => $date,
        'page-title' => $page_title,
        'category_field' => $content['category_field'],
        'tag_field' => $content['tag_field'],
      );
      $this->nodeGeneratorNodeCreation($params, $context);
    }
  }

  /**
   * Download images from remote server to local server.
   */
  public function nodeGeneratorGetImageFromUri($html, $content) {
    global $base_url;
    foreach ($html->find('img') as $element) {
      $result = parse_url($content);
      $base = $result['scheme'] . "://" . $result['host'];
      $starts_with = substr($element->src, 0, 1);
      if (stripos($element->src, 'http') !== 0) {
        if ($starts_with != '/') {
          $source = $base . '/' . $element->src;
        } else {
          $source = $base . $element->src;
        }
      } else {
        $source = $element->src;
      }
      $path_parts = pathinfo($element->src);
      $image_name = $path_parts['filename'] . '.' . $path_parts['extension'];
      $destination = 'public://' . $image_name;
      $copy = $this->nodeGeneratorGrabFiles($source, $destination);
      if ($copy) {
        $uri = 'public://';
        $path_file = file_create_url($uri);
        $p = str_replace($base_url, "", $path_file);
        $element->src = $path_file . $image_name;
      }
    }
  }

  /**
   * Get images from url and save them on our server. 
   */
  public function nodeGeneratorGrabFiles($url, $saveto) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
    $raw = curl_exec($ch);
    curl_close($ch);
    if (file_exists($saveto)) {
      unlink($saveto);
    }
    $fp = fopen($saveto, 'x');
    fwrite($fp, $raw);
    fclose($fp);
    return TRUE;
  }

  /**
   * Download images from remote server to local server.
   */
  public function nodeGeneratorGetFilesFromHref($html, $content) {
    //watchdog('actions', 'Action.', array());
    global $base_url;
    foreach ($html->find('a') as $element) {
      $path_parts = pathinfo($element->href);
      if ($path_parts['extension']) {
        $file_name = $path_parts['filename'] . '.' . $path_parts['extension'];
        if ($path_parts['extension'] == 'pdf' || $path_parts['extension'] == 'doc') {
          $result = parse_url($content);
          $base = $result['scheme'] . "://" . $result['host'];
          $starts_with = substr($element->href, 0, 1);
          if (stripos($element->href, 'http') !== 0) {
            if ($starts_with != '/') {
              $source = $base . '/' . $element->href;
            } else {
              $source = $base . $element->href;
            }
          } else {
            $source = $element->href;
          }
          $destination = 'public://' . $file_name;
          $copy = $this->nodeGeneratorGrabFiles($source, $destination);
          if ($copy) {
            $uri = 'public://';
            $path_file = file_create_url($uri);
            $p = str_replace($base_url, "", $path_file);
            $element->href = $path_file . $file_name;
          }
        }
      }
    }
  }

  /**
   * Get metatags from url.
   */
  function nodeGeneratorGetMetatagsUrl($url) {
    $metatagss = array();
    $htmll = $this->nodeGeneratorGetContentFromXmlLoc($url);
    $doc = new DOMDocument();
    @$doc->loadHTML($htmll);
    $nodes = $doc->getElementsByTagName('title');
    $title = $nodes->item(0)->nodeValue;
    $metas = $doc->getElementsByTagName('meta');
    $description = '';
    $keywords = '';
    for ($i = 0; $i < $metas->length; $i++) {
      $meta = $metas->item($i);
      if ($meta->getAttribute('name') == 'description' || $meta->getAttribute('name') == 'Description')
        $description = $meta->getAttribute('content');
      if ($meta->getAttribute('name') == 'keywords')
        $keywords = $meta->getAttribute('content');
      if ($meta->getAttribute('language') == 'language')
        ;
      $language = $meta->getAttribute('language');
    }
    $metatagss['description'] = preg_replace("/&#?[a-z0-9]{2,8};/i", "", $description);
    $metatagss['keywords'] = html_entity_decode($keywords);
    $metatagss['title'] = html_entity_decode($title);
    return $metatagss;
  }

  /**
   * Method to generate metatag list supported by metatag module.
   */
  public function nodeGeneratorGetMetatagsList() {
    $tags = metatag_get_info();
    $taglist = array();
    foreach ($tags['tags'] as $key => $val) {
      $taglist[] = $tags['tags'][$key]['name'];
    }
    return $taglist;
  }

  /**
   * Method to adjust the robot metatags.
   */
  public function nodeGeneratorAdjustRobotTags($robot_list) {
    $tags = array();
    foreach (explode(',', $robot_list) as $key => $val) {
      $tags[trim($val)] = trim($val);
    }
    return $tags;
  }

  /**
   * Replace absolute anchor paths in body to our server paths. 
   */
  public function nodeGeneratorReplaceDomainAnchor($body_html, $url) {
    $result = parse_url($url);
    $base = $result['scheme'] . "://" . $result['host'];
    $base_url = 'href="' . $base . '/';
    $body = str_replace($base_url, 'href="/', $body_html);
    return $body;
  }

  /**
   * Node generator clean url alias.
   */
  public function nodeGeneratorGenerateAlias($string) {
    $substr = substr($url, strpos($url, '//') + 2);
    if (strpos($substr, '/') === FALSE) {
      return '';
    }
    $alias = str_replace(' ', '-', substr($substr, strpos($substr, '/') + 1));
    return $alias;
  }

  /**
   * Method to create the new node of specified content type.
   */
  public function nodeGeneratorNodeCreation($content, &$context) {
//    kint($content['type']);
//    die;
    $node = Node::create(array(
        'type' => $content['type'],
        'title' => $content['page_title'],
        'uid' => '1',
        'status' => 1,
        'path' => '/' . $content['alias'],
    ));
    $node->body->value = $content['body'];
    $node->body->format = 'full_html';
    $node->save();
  }

}
